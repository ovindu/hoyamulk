<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/hoyamulk/admin/');
define('HTTP_CATALOG', 'http://localhost/hoyamulk/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/hoyamulk/admin/');
define('HTTPS_CATALOG', 'http://localhost/hoyamulk/');

// DIR
define('DIR_APPLICATION', 'D:/PROGRAMMING/wamp/www/hoyamulk/admin/');
define('DIR_SYSTEM', 'D:/PROGRAMMING/wamp/www/hoyamulk/system/');
define('DIR_IMAGE', 'D:/PROGRAMMING/wamp/www/hoyamulk/image/');
define('DIR_STORAGE', 'D:/PROGRAMMING/wamp/www/hoyamulk/storage/');
define('DIR_CATALOG', 'D:/PROGRAMMING/wamp/www/hoyamulk/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'hoyamulk_db');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
