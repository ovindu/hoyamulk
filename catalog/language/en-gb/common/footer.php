<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_solution_by']	= ' | Solution By <a target="_blank" href="https://www.facebook.com/3decenttech"> 3DecentTech</a>';
$_['text_credits']		= '<a href="%s"> %s</a> | Hoyamu Lanka Pvt Ltd, &copy; %s';